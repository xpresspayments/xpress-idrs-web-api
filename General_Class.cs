﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Xpress.DRS
{
    public class General_Class
    {

        public string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        private byte[] CalculateSHA256(string str)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] hashValue;
            UTF8Encoding objUtf8 = new UTF8Encoding();
            hashValue = sha256.ComputeHash(objUtf8.GetBytes(str));

            return hashValue;
        }

        public string updateWithBatchID()
        {
            Random rnd = new Random();
            var batchID = rnd.Next(111, 9999999).ToString() + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstring"].ConnectionString);
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("update top (1000) tbl_disputeResolution set batchID = '" + batchID + "' WHERE responseCode IS NULL or responseCode=''", cn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            finally
            {
                cn.Close();cn.Dispose();
            }
            return batchID;
        }

        public void updateWithResponse(string responseCode, string responseMessage, string batchID, string query)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstring"].ConnectionString);
            try
            {
                cn.Open();
                //   SqlCommand cmd = new SqlCommand("update top (1000) tbl_disputeResolution set responseCode = '" + responseCode + "',responseMessage='" + responseMessage + "' WHERE batchID='" + batchID + "'", cn);
                // SqlCommand cmd = new SqlCommand("update tbl_disputeResolution set responseCode = '" + responseCode + "',responseMessage='" + responseMessage + "' WHERE batchID='" + batchID + "'", cn);
                SqlCommand cmd = new SqlCommand(query, cn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            finally
            {
                cn.Close(); cn.Dispose();
            }
        }

        public int countAllPendingRecord()
        {
            int batchCount = 0;
            string recordCount = "0";
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstring"].ConnectionString);
            try
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SELECT COUNT(accountNumber) from tbl_disputeResolution WHERE responseCode IS NULL or responseCode=''", cn);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    recordCount = (string)dr.GetValue(0).ToString();
                }

                if (Convert.ToInt32(recordCount) <= 1000)
                {
                    batchCount = 1;
                }
                else
                {
                    batchCount = (Convert.ToInt32(recordCount) / 1000);
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
            finally
            {
                cn.Close(); cn.Dispose();
            }
            return batchCount;
        }


    }
}