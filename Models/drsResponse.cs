﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.DRS.Models
{
    public class drsResponse
    {
        public string status { get; set; }
        public string responseCode { get; set; }
        public string message { get; set; }
      //  [JsonProperty(PropertyName = "dataObject")]
      //  public dataObject dataObject1 { get; set; }
        public List<pertialJoin> data { get; set; }
        public string socket { get; set; }
    }

    public class pertialJoin
    {
        public dataObject dataObject { get; set; }
        public error error { get; set; }
    }

    public class dataObject
    {
        public string accountName { get; set; }
        public string accountNumber { get; set; }
        public string acquirerBank { get; set; }
        public string issuingBank { get; set; }
        public string acquirerRRN { get; set; }
        public string amount { get; set; }
        public string approvalAuthorizationCode { get; set; }
        public string ARN { get; set; }
        public string atmLocation { get; set; }
        public string dateOfTransaction { get; set; }
        public string dateOfSettlement { get; set; }
        public string issuerRRN { get; set; }
        public string processorIDTransactionID { get; set; }
        public string merchantName { get; set; }
        public string merchantID { get; set; }
        public string maskedPAN { get; set; }
        public string STAN { get; set; }
        public string terminalID { get; set; }
        public string transactionChannelID { get; set; }
        public string processorID { get; set; }
        public string otherDetails { get; set; }      
    }

    public class error
    {
        public string accountName { get; set; }
        public string accountNumber { get; set; }
        public string acquirerBank { get; set; }
        public string issuingBank { get; set; }
        public string acquirerRRN { get; set; }
        public string amount { get; set; }
        public string approvalAuthorizationCode { get; set; }
        public string ARN { get; set; }
        public string atmLocation { get; set; }
        public string dateOfTransaction { get; set; }
        public string dateOfSettlement { get; set; }
        public string issuerRRN { get; set; }
        public string processorIDTransactionID { get; set; }
        public string merchantName { get; set; }
        public string merchantID { get; set; }
        public string maskedPAN { get; set; }
        public string STAN { get; set; }
        public string terminalID { get; set; }
        public string transactionChannelID { get; set; }
        public string processorID { get; set; }
        public string otherDetails { get; set; }
    }

}
