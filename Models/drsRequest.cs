﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.DRS.Models
{
    public class drsRequest
    {
        public string accountName { get; set; }
        public string accountNumber { get; set; }
        public string acquirerBank { get; set; }
        public string issuingBank { get; set; }
        public string acquirerRRN { get; set; }
        public string amount { get; set; }
        public string approvalAuthorizationCode { get; set; }
        public string ARN { get; set; }
        public string atmLocation { get; set; }
        public string dateOfTransaction { get; set; }
        public string dateOfSettlement { get; set; }
        public string issuerRRN { get; set; }
      //  [JsonProperty(PropertyName = "processorID-transactionID")]
        public string processorIDTransactionID { get; set; }
        public string merchantName { get; set; }
        public string merchantID { get; set; }
        public string maskedPAN { get; set; }
        public string STAN { get; set; }
        public string terminalID { get; set; }
        public string transactionChannelID { get; set; }
        public string processorID { get; set; }
        public string otherDetails { get; set; }
    }

   /* public class otherDetails
    {      
      public string version { get; set; }
        public string email { get; set; }
    }*/

    public class data
    {
        [JsonProperty(PropertyName = "data")]
        public string data1 { get; set; }
    }

}