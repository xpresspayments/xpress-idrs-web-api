﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Data.SqlClient;
using NLog;
using Newtonsoft;
using Newtonsoft.Json;
using System.Data;
using Xpress.DRS.Models;
using System.IO;
using NLog;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Text;

namespace Xpress.DRS.Controllers
{
    public class DRMSController : ApiController
    {
        General_Class gclass = new General_Class();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        // GET: api/DRMS
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/DRMS/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/DRMS
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/DRMS/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DRMS/5
        public void Delete(int id)
        {
        }

        [HttpPost]
        [Route("api/v1/disputeResol")]
        public async Task<IHttpActionResult> disputeResol()
        {
            try
            {
                if (!Directory.Exists("c:\\LOG-DRMS"))
                {
                    Directory.CreateDirectory("c:\\LOG-DRMS");
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }


            int batchCount = gclass.countAllPendingRecord();

            for (int j = 0; j < batchCount; j++)
            {

                String batchID = gclass.updateWithBatchID();

                try
                {

                    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstring"].ConnectionString);
                    cn.Open();
                    string query = "SELECT top 1000* FROM tbl_disputeResolution where responseCode IS NULL";
                    SqlDataAdapter da = new SqlDataAdapter(query, cn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    List<drsRequest> request = new List<drsRequest>();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        drsRequest req1 = new drsRequest();
                        req1.accountName = dt.Rows[i]["accountName"].ToString();
                        req1.accountNumber = dt.Rows[i]["accountNumber"].ToString();
                        req1.acquirerBank = dt.Rows[i]["acquirerBank"].ToString();

                        req1.issuingBank = dt.Rows[i]["issuingBank"].ToString();
                        req1.acquirerRRN = dt.Rows[i]["acquirerRRN"].ToString();
                        req1.amount = dt.Rows[i]["amount"].ToString();
                        req1.atmLocation = dt.Rows[i]["atmLocation"].ToString();
                        req1.ARN = dt.Rows[i]["ARN"].ToString();
                        req1.approvalAuthorizationCode = dt.Rows[i]["approvalAuthorizationCode"].ToString();
                        req1.dateOfTransaction = dt.Rows[i]["dateOfTransaction"].ToString();
                        req1.dateOfSettlement = dt.Rows[i]["dateOfSettlement"].ToString();
                        req1.issuerRRN = dt.Rows[i]["issuerRRN"].ToString();
                        req1.processorIDTransactionID = dt.Rows[i]["processorIDtransactionID"].ToString();
                        req1.merchantName = dt.Rows[i]["merchantName"].ToString();
                        req1.merchantID = dt.Rows[i]["merchantID"].ToString();
                        req1.maskedPAN = dt.Rows[i]["maskedPAN"].ToString();
                        req1.STAN = dt.Rows[i]["STAN"].ToString();
                        req1.terminalID = dt.Rows[i]["terminalID"].ToString();
                        req1.transactionChannelID = dt.Rows[i]["transactionChannelID"].ToString();
                        req1.processorID = dt.Rows[i]["processorID"].ToString();
                        /* otherDetails reqOtherDetails = new otherDetails();
                         reqOtherDetails.version = ConfigurationManager.AppSettings["version"];
                         reqOtherDetails.email = dt.Rows[i]["email"].ToString();
                         req1.otherDetails = reqOtherDetails;*/
                        request.Add(req1);
                        // string abs = dt.Rows[i]["accountName"].ToString();
                    }

                    var incomingReq = JsonConvert.SerializeObject(request);

                    var institutionCode = ConfigurationManager.AppSettings["institutionCode"];
                    var password = ConfigurationManager.AppSettings["password"];
                    var signature_meth = ConfigurationManager.AppSettings["SIGNATURE_METH"];

                    var byteArray = Encoding.ASCII.GetBytes($"{institutionCode}:{password}");
                    var conc = institutionCode + DateTime.Now.Year + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + password;
                    var signature = gclass.ComputeSha256Hash(conc);

                    var baseUrl = ConfigurationManager.AppSettings["baseUrl"];
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(baseUrl);
                        string token = Convert.ToBase64String(byteArray);
                        client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);
                        client.DefaultRequestHeaders.Add("SIGNATURE_METH", signature_meth);
                        client.DefaultRequestHeaders.Add("SIGNATURE", signature);
                        client.DefaultRequestHeaders.Accept.Add(
                           new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Accept.Add(
                          new MediaTypeWithQualityHeaderValue("text/plain"));

                        var data = new data
                        {
                            data1 = incomingReq
                        };

                        var requestToNibbs = JsonConvert.SerializeObject(data).Replace("\"[", "[").Replace("]\"", "]").Replace(@"\", "");
                        logger.Info("------------------------------------------------------------------------------------------------------------------------------------------\n\n" + "REQUEST TO NIBBS IDRS\n"+requestToNibbs + "\n\n");

                        //StringContent content = new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"); //new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"));
                        HttpResponseMessage response = await client.PostAsync("transaction", new StringContent(requestToNibbs, Encoding.UTF8, "application/json"));  //.Result;

                        var content = await response.Content.ReadAsStringAsync();
                        var responseFromDRS = JsonConvert.DeserializeObject<drsResponse>(content);
                        string asp = content;
                        logger.Info("\n\nRESPONSE FROM NIBBS IDRS \n\n" + JsonConvert.SerializeObject(responseFromDRS).Replace(@"\", "") + "\n\n------------------------------------------------------------------------------------------------------------------------------------------\n\n");

                        foreach (var item in responseFromDRS.data)
                        {
                            string Updatequery = "UPDATE tbl_disputeResolution set responseCode='01',responseMessage='" + item.error.accountName + item.error.accountNumber + item.error.acquirerBank + item.error.acquirerRRN + item.error.amount + item.error.approvalAuthorizationCode + item.error.ARN + item.error.atmLocation + item.error.dateOfSettlement + item.error.dateOfTransaction + item.error.issuerRRN + item.error.issuingBank + item.error.maskedPAN + item.error.merchantID + item.error.merchantName + item.error.processorID + item.error.processorIDTransactionID + item.error.STAN + item.error.terminalID + item.error.transactionChannelID + item.error.otherDetails + "' WHERE accountNumber='" + item.dataObject.accountNumber + "' AND processorIDtransactionID='" + item.dataObject.processorIDTransactionID + "' and dateOfTransaction='" + item.dataObject.dateOfTransaction + "' and batchID='" + batchID + "' ";
                            gclass.updateWithResponse(responseFromDRS.responseCode, responseFromDRS.message, batchID, Updatequery);
                        }

                        string Updatequery1 = "UPDATE tbl_disputeResolution set responseCode='00',responseMessage='" + responseFromDRS.message + "' WHERE responseCode IS NULL and batchID='" + batchID + "' ";
                        gclass.updateWithResponse(responseFromDRS.responseCode, responseFromDRS.message, batchID, Updatequery1);


                        return Ok(responseFromDRS);
                    }
                }
                catch (Exception ex)
                {
                    logger.Trace("\n" + ex.Message + "\n");
                    return Ok(ex.Message);
                }

                

            }
            return Ok();
        }

        [HttpPost]
        [Route("api/v1/reset")]
        public async Task<IHttpActionResult> disputeReset()
        {
            var headerKey = Request.Headers.GetValues("apiKey").First();
            var authorizationKey = ConfigurationManager.AppSettings["Internal-apiKey"]; //Request.Headers.GetValues("apiKey");//Internal-apiKey

            if (Request.Headers == null)
            {
                return BadRequest("Authentication Key is Required!");
            }
            else if (headerKey.ToString() == null)
            {
                return BadRequest("Authentication Key is Required!");
            }
            else if (authorizationKey.ToString() != headerKey.ToString())
            {
                return BadRequest("Invalid Authentication Key!");
            }
            else
            {

                try
                {
                    var institutionCode = ConfigurationManager.AppSettings["institutionCode"];
                    var password = ConfigurationManager.AppSettings["password"];

                    var byteArray = Encoding.ASCII.GetBytes($"{institutionCode}:{password}");
                    String conc = institutionCode + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + password;
                    var signature = gclass.ComputeSha256Hash(conc);

                    var baseUrl = ConfigurationManager.AppSettings["baseUrl"];
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(baseUrl);
                        string token = Convert.ToBase64String(byteArray);
                        client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);
                        client.DefaultRequestHeaders.Accept.Add(
                           new MediaTypeWithQualityHeaderValue("application/json"));

                        var foo = new StringContent("");
                        var stringContent = new StringContent(string.Empty);
                        HttpResponseMessage response = await client.PostAsync("reset", stringContent);  //.Result;

                        //HttpResponseMessage response = await client.PostAsync("reset", new StringContent("", Encoding.UTF8, "application/json"));  //.Result;

                        var content = await response.Content.ReadAsStringAsync();

                        logger.Info("\n\n RESPONSE FROM NIBBS IDRS \n\n" + content.ToString() + "\n\n------------------------------------------------------------------------------------------------------------------------------------------\n\n");


                        var resetResponse = new drsResetResponse
                        {
                            ResponseCode = response.Headers.GetValues("ResponseCode").FirstOrDefault(),
                            APIKey = response.Headers.GetValues("APIKey").FirstOrDefault(),
                            IVKey = response.Headers.GetValues("IVKey").FirstOrDefault(),
                            Password = response.Headers.GetValues("Password").FirstOrDefault()
                        };


                        // var responseFromDRS = JsonConvert.DeserializeObject<drsResponse>(content);

                        return Ok(resetResponse);
                    }
                }
                catch (Exception ex)
                {
                    return Ok(ex.Message);
                }

            }

        }

    }


}
